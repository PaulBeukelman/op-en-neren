import React from "react";
import { Player, Round } from "../typings";
import { evaluateContract } from "../utils/evaluateContract";
import { getOrderedPlayers } from "../utils/getPlayerOrder";
import { replaceRound } from "../utils/replaceRound";

export interface UseGame {
  rounds: Round[];
  currentRound: Round;
  players: Player[];
  updateRound(
    round: Round,
    playerName: string,
    field: "bid" | "score",
    value: number
  ): void;
  newGame(players: Player[], maxCards: number): void;
  nextRound(): void;
  prevRound(): void;
}

interface GameStateStorage {
  version: number;
  rounds: Round[];
  players: Player[];
  currentRoundNumber: number;
}

export const LS_KEY_GAME_STATE = "game_state";

export const useGame = (): UseGame => {
  const [rounds, setRounds] = React.useState<Round[]>([]);
  const [players, setPlayers] = React.useState<Player[]>([]);
  const [currentRoundNumber, setCurrentRoundNumber] = React.useState(0);
  const totalRounds = rounds?.length || 0;

  /**
   * Load progress on mount
   */
  React.useEffect(() => {
    loadProgress();
  }, []);

  /**
   * Save progress on change
   */
  React.useEffect(() => {
    if (!rounds.length) {
      return;
    }
    const gameState: GameStateStorage = {
      version: 1,
      rounds,
      players,
      currentRoundNumber,
    };
    localStorage.setItem(LS_KEY_GAME_STATE, JSON.stringify(gameState));
  }, [rounds, players, currentRoundNumber]);

  /**
   * Initialize new game
   */
  const newGame = (players: Player[], maxCards: number): void => {
    // delete old storage
    localStorage.removeItem(LS_KEY_GAME_STATE);
    const up = Array.from(Array(maxCards).keys()).map((_, i) => i + 1);
    const down = Array.from(Array(maxCards).keys()).map(
      (_, i) => maxCards - (i + 1) + 1
    );
    setCurrentRoundNumber(0);
    setRounds(
      [...up, ...down].map((cardCount, index) => {
        return {
          index,
          humanIndex: index + 1,
          cardCount,
          contracts: null,
          playerOrder: getOrderedPlayers(index, players),
        };
      })
    );
    setPlayers(players);
  };

  const updateRound = (
    round: Round,
    playerName: string,
    field: "bid" | "score",
    value: number
  ) => {
    const newContract = {
      ...round.contracts?.[playerName],
      [field]: value,
    };
    const updatedRound = {
      ...round,
      contracts: {
        ...(round.contracts || {}),
        [playerName]: {
          ...newContract,
          ...evaluateContract(newContract),
        },
      },
    };
    setRounds(replaceRound(updatedRound, rounds));
  };

  const nextRound = (): void => {
    const next = currentRoundNumber + 1;
    if (next > totalRounds - 1) {
      return;
    }
    document.querySelector(`.col-${next}`)?.scrollIntoView({
      behavior: "smooth",
      inline: "center",
      block: "center",
    });
    setCurrentRoundNumber(next);
  };

  const prevRound = (): void => {
    const prev = currentRoundNumber - 1;
    if (prev < 0) {
      return;
    }
    document.querySelector(`.col-${prev}`)?.scrollIntoView({
      behavior: "smooth",
      inline: "center",
      block: "center",
    });
    setCurrentRoundNumber(prev);
  };

  const loadProgress = (): boolean => {
    let gameState = localStorage.getItem(LS_KEY_GAME_STATE);
    if (!gameState) {
      return false;
    }
    const { rounds, players, currentRoundNumber } = JSON.parse(
      gameState
    ) as GameStateStorage;
    setRounds(rounds);
    setPlayers(players);
    setCurrentRoundNumber(currentRoundNumber);
    return true;
  };

  return {
    rounds,
    currentRound: rounds[currentRoundNumber],
    players,
    updateRound,
    nextRound,
    prevRound,
    newGame,
  };
};
