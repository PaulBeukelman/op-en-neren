export type AllowedPlayerCount = 2 | 3 | 4 | 5 | 6;

export interface Player {
  name: string;
  color: string;
}

export interface EndResultPlayer extends Player {
  endResult: number;
}

export interface Contract {
  bid?: number;
  score?: number;
  isCorrect?: boolean;
  points?: number;
}

export interface Round {
  /**
   * 0 1 2 ... 17 18 19
   */
  index: number;
  /**
   * 1 2 3 ... 18 19 20
   */
  humanIndex: number;
  /**
   * 1 2 3 ... 3 2 1
   */
  cardCount: number;
  contracts: Record<string, Contract> | null;
  playerOrder: Player[];
}
