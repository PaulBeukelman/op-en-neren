import {
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  LineElement,
  PointElement,
  Title,
  Tooltip,
} from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import React from "react";
import { Line } from "react-chartjs-2";
import { Round } from "../typings";
import { getDataPointsForPlayer } from "../utils/getDataPoints";

ChartJS.register(
  CategoryScale,
  ChartDataLabels,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

interface IContext {
  dataIndex?: number;
  dataset?: { data: any; borderColor?: any };
}

const options = {
  responsive: true,
  plugins: {
    legend: {
      display: false,
    },
    title: {
      display: false,
    },
    tooltip: {
      enabled: false,
    },
    datalabels: {
      borderRadius: 4,
      backgroundColor: function (context: IContext) {
        if (context.dataIndex === (context.dataset?.data.length || 1) - 1) {
          return context.dataset?.borderColor;
        }
        return "transparent";
      },
      color: function (context: IContext) {
        if (context.dataIndex === (context.dataset?.data.length || 1) - 1) {
          return "white";
        }
        return "transparent";
      },
      padding: 6,
    },
  },
};

interface Props {
  rounds: Round[];
}

export const Chart: React.FC<Props> = ({ rounds }) => {
  const players = rounds[0]?.playerOrder;
  if (players.length === 0) {
    return null;
  }
  const datasetDefault = {
    borderWidth: 5,
    tension: 0.5,
  };
  const datasets = players.map((player) => {
    return {
      ...datasetDefault,
      label: player.name,
      borderColor: player.color || "red",
      data: getDataPointsForPlayer(rounds, player.name),
    };
  });
  const data = {
    labels: rounds.map((round) => round.cardCount),
    datasets,
  };
  return (
    <>
      <div className="mt-4">
        <h1 className="main-title">Stand:</h1>
      </div>
      <Line options={options} data={data} />
    </>
  );
};
