import { Button, Label, Modal, TextInput } from "flowbite-react";
import React from "react";
import { AllowedPlayerCount, Player } from "../typings";

interface Props {
  newGame(players: Player[], maxCards: number): void;
}

type MaxCards = 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;

const playerColors = ["red", "blue", "orange", "purple", "yellow", "green"];

export const NewGame: React.FC<Props> = ({ newGame }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [players, setPlayers] = React.useState<Player[]>([]);
  const [maxCards, setMaxCards] = React.useState<MaxCards>(10);
  const [configPage, setConfigPage] = React.useState<1 | 2>(1);
  const maxCardsPP = {
    2: 12,
    3: 12,
    4: 12,
    5: 10,
    6: 8,
  }[(players?.length as AllowedPlayerCount) || 2];

  const updatePlayers = (count: AllowedPlayerCount) => {
    if (!players?.length || players.length !== count) {
      setPlayers(
        Array.from(Array(count).keys()).map((_, i) => {
          return {
            color: playerColors[i],
            name: players?.[i]?.name || "",
          };
        })
      );
    }
  };

  const updatePlayerName = (index: number, name: string) => {
    setPlayers(
      players?.map((player, i) => {
        if (i === index) {
          return {
            ...player,
            name,
          };
        }
        return player;
      })
    );
  };

  React.useEffect(() => {
    if (maxCards > maxCardsPP) {
      setMaxCards(maxCardsPP as MaxCards);
    }
    if (!players?.length) {
      updatePlayers(2);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [players?.length]);

  const notValid = players?.some((player) => !player.name);

  return (
    <React.Fragment>
      <Button onClick={() => setIsOpen(true)}>Nieuw spel</Button>
      <Modal
        show={isOpen}
        size="md"
        popup={true}
        onClose={() => setIsOpen(false)}
      >
        <Modal.Header />
        <Modal.Body>
          {configPage === 1 && (
            <div>
              <div>
                <div className="mb-2 block">
                  <Label htmlFor="players" value="Hoeveel spelers?" />
                </div>
                <TextInput
                  id="players"
                  min={2}
                  max={6}
                  type="number"
                  value={players?.length}
                  onChange={(e) =>
                    updatePlayers(Number(e.target.value) as AllowedPlayerCount)
                  }
                  required={true}
                />
              </div>
              <div>
                <div className="mb-2 block">
                  <Label htmlFor="rounds" value="Hoeveel rondes?" />
                </div>
                <TextInput
                  id="rounds"
                  type="number"
                  value={maxCards}
                  min={5}
                  max={maxCardsPP}
                  onChange={(e) =>
                    setMaxCards(Number(e.target.value) as MaxCards)
                  }
                  required={true}
                />
              </div>
              <div className="flex justify-center gap-4 mt-4">
                <Button color={"failure"} onClick={() => setIsOpen(false)}>
                  Annuleren
                </Button>
                <Button color={"success"} onClick={() => setConfigPage(2)}>
                  Verder
                </Button>
              </div>
            </div>
          )}
          {configPage === 2 && (
            <div>
              <div>
                {players?.map((player, i) => {
                  return (
                    <React.Fragment key={i}>
                      <Label
                        htmlFor={`player_${i}`}
                        value={`Naam speler ${i + 1}`}
                      />
                      <TextInput
                        id={`player_${i}`}
                        value={player.name}
                        required={true}
                        onChange={(e) => updatePlayerName(i, e.target.value)}
                      />
                    </React.Fragment>
                  );
                })}
              </div>
              <div className="flex justify-center gap-4 mt-4">
                <Button color={"failure"} onClick={() => setConfigPage(1)}>
                  Terug
                </Button>
                <Button
                  disabled={notValid}
                  color={"success"}
                  onClick={() => {
                    newGame(players, maxCards);
                    setIsOpen(false);
                    updatePlayers(2);
                    setConfigPage(1);
                  }}
                >
                  Klaar
                </Button>
              </div>
            </div>
          )}
        </Modal.Body>
      </Modal>
    </React.Fragment>
  );
};
