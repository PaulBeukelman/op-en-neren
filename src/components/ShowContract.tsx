import React from "react";
import { Contract } from "../typings";

interface Props {
  contract?: Contract;
}

export const ShowContract: React.FC<Props> = ({ contract }) => {
  const wins = [contract?.bid, contract?.score].filter(
    (win) => typeof win !== "undefined"
  );
  return (
    <>
      <div className="whitespace-nowrap">{wins.join(" / ")}</div>
      <div className="pt-4">{contract?.points}</div>
    </>
  );
};
