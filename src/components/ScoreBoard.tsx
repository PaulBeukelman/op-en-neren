import cn from "classnames";
import { Button, Table } from "flowbite-react";
import React from "react";
import { UseGame } from "../hooks/useGame";
import { Chart } from "./Chart";
import { EndResult } from "./EndResult";
import { RoundAdmin } from "./RoundAdmin";
import { ShowContract } from "./ShowContract";

interface Props {
  game: UseGame;
}

export const ScoreBoard: React.FC<Props> = ({ game }) => {
  const [endResultOpen, setEndResultOpen] = React.useState(false);
  if (!game.rounds.length) {
    return null;
  }

  return (
    <>
      <Table>
        <Table.Head>
          <Table.HeadCell className="bg-gray-50 sticky left-0 top-0">
            <div className="pl-5 hidden md:block">Ronde</div>
            <div className="pl-5 md:hidden">R</div>
          </Table.HeadCell>
          {game.rounds?.map((r, i) => {
            return (
              <Table.HeadCell
                key={r.index}
                className={cn(
                  {
                    "bg-gray-200": game.currentRound.index === i,
                  },
                  "text-center",
                  "min-w-[80px]",
                  `col-${r.index}`
                )}
              >
                {r.cardCount}
              </Table.HeadCell>
            );
          })}
        </Table.Head>
        <Table.Body>
          {game.players.map((player) => {
            return (
              <React.Fragment key={player.name}>
                <Table.Row className={"border-t-4 border-gray-200"}>
                  <Table.Cell className="pl-6 sticky left-0 top-0 bg-white">
                    <div
                      className="-m-4 absolute left-8 bottom-5 w-0"
                      style={{
                        transform: "translate3d(0,50%,0) rotate(270deg)",
                      }}
                    >
                      <div
                        style={{ backgroundColor: player.color }}
                        className="text-white uppercase overflow-hidden w-20 text-center py-1 rounded-b-lg px-4 text-ellipsis"
                      >
                        {player.name}
                      </div>
                    </div>
                    <div className="pl-5 hidden md:block">Slagen</div>
                    <div className="pt-4 hidden md:block pl-5">Punten</div>
                    <div className="pl-5 md:hidden">S</div>
                    <div className="pt-4 md:hidden pl-5">P</div>
                  </Table.Cell>
                  {game.rounds?.map((r, i) => {
                    const active = game.currentRound.index === i;
                    const contract = r.contracts?.[player.name];
                    return (
                      <Table.Cell
                        key={r.index}
                        className={cn(
                          {
                            "bg-green-100":
                              !active &&
                              contract?.isCorrect &&
                              contract?.points,
                          },
                          {
                            "bg-red-100":
                              !active &&
                              !contract?.isCorrect &&
                              contract?.points,
                          },
                          { "bg-gray-200": active },
                          "border-x-4 border-gray-200 text-center"
                        )}
                      >
                        <ShowContract contract={contract} />
                      </Table.Cell>
                    );
                  })}
                </Table.Row>
              </React.Fragment>
            );
          })}
        </Table.Body>
      </Table>
      <div className="mt-4">
        <h1 className="main-title">Ronde opties:</h1>
      </div>
      <div className="mt-2 flex justify-between">
        <Button onClick={game.prevRound}>Vorige</Button>
        <RoundAdmin game={game} />
        {game.currentRound.index === (game?.rounds.length - 1 || 0) && (
          <Button onClick={() => setEndResultOpen(true)}>Uitslag</Button>
        )}
        {game.currentRound.index !== (game?.rounds.length - 1 || 0) && (
          <Button onClick={game.nextRound}>Volgende</Button>
        )}
      </div>
      <div className="mt-4">
        <Chart rounds={game.rounds} />
      </div>
      <EndResult
        rounds={game.rounds}
        isOpen={endResultOpen}
        close={() => setEndResultOpen(false)}
      />
    </>
  );
};
