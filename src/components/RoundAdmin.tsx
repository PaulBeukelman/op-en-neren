import { Button, Label, Modal, TextInput } from "flowbite-react";
import React from "react";
import { UseGame } from "../hooks/useGame";
import { getForbiddenBid } from "../utils/getForbiddenBid";

interface Props {
  game?: UseGame;
}

export const RoundAdmin: React.FC<Props> = ({ game }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const round = game?.currentRound;

  if (!round) {
    return null;
  }
  const forbiddenBid = getForbiddenBid(round);
  const forbiddenBidText = `Mag geen ${forbiddenBid} bieden`;

  const nextRoundAdmin = () => {
    // Easy short cut for getting the input state right
    // and we can simply rely on state of the current round
    setIsOpen(false);
    game.nextRound();
    setTimeout(() => {
      setIsOpen(true);
    }, 250);
  };

  if (!isOpen) {
    return <Button onClick={() => setIsOpen(true)}>Invoeren</Button>;
  }
  return (
    <>
      <Button onClick={() => setIsOpen(true)}>Invoeren</Button>
      <Modal
        show={isOpen}
        size="md"
        popup={true}
        onClose={() => setIsOpen(false)}
      >
        <Modal.Header>
          <div className="px-4">Kaarten: {round.cardCount}</div>
        </Modal.Header>
        <Modal.Body>
          <div className="round-input-fields">
            {round.playerOrder.map((player, i) => {
              const contract = round.contracts?.[player?.name];
              let bidDisabled = false;
              if (i > 0) {
                const prevPlayer = round.playerOrder[i - 1];
                const prevContract = round.contracts?.[prevPlayer.name];
                bidDisabled = typeof prevContract?.bid === "undefined";
              }
              const showForbidden =
                round.playerOrder.length - 1 === i && forbiddenBid !== null;
              return (
                <div className={"pb-4"} key={player?.name}>
                  <div>
                    {i + 1}) {player?.name}
                  </div>
                  <div className="flex justify-between gap-2">
                    <div>
                      <Label htmlFor={`${player}_bid`} value="Bied" />
                      <TextInput
                        id={`${player.name}_bid`}
                        type="number"
                        tabIndex={i + 1}
                        value={contract?.bid}
                        onChange={(e) =>
                          game.updateRound(
                            round,
                            player.name,
                            "bid",
                            Number(e.target.value)
                          )
                        }
                        disabled={bidDisabled}
                        max={round.cardCount}
                        required={true}
                        helperText={
                          showForbidden ? forbiddenBidText : undefined
                        }
                        addon={
                          showForbidden &&
                          contract?.bid === forbiddenBid && (
                            <div className="font-bold">!</div>
                          )
                        }
                      />
                    </div>
                    <div>
                      <Label htmlFor={`${player}_score`} value="Resultaat" />
                      <TextInput
                        id={`${player}_score`}
                        type="number"
                        tabIndex={i + 1 + round.playerOrder.length}
                        value={contract?.score}
                        max={round.cardCount}
                        onChange={(e) =>
                          game.updateRound(
                            round,
                            player.name,
                            "score",
                            Number(e.target.value)
                          )
                        }
                        disabled={typeof contract?.bid === "undefined"}
                        required={true}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="mt-2 flex justify-between">
            <Button color={"info"} onClick={() => setIsOpen(false)}>
              Ok
            </Button>
            {game.rounds.length - 1 > game.currentRound.index && (
              <Button color={"info"} onClick={nextRoundAdmin}>
                Volgende ronde
              </Button>
            )}
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};
