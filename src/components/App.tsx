import React from "react";
import { useGame } from "../hooks/useGame";
import { NewGame } from "./NewGame";
import { ScoreBoard } from "./ScoreBoard";

export const App: React.FC = () => {
  const game = useGame();
  return (
    <div className="container py-10 px-4 mx-auto">
      <div className="mb-4 md:flex justify-between">
        <h1 className="main-title">Scoreboard</h1>
        <div className="hidden md:block">
          <NewGame newGame={game.newGame} />
        </div>
      </div>
      <ScoreBoard game={game} />
      <div className="md:hidden">
        <NewGame newGame={game.newGame} />
      </div>
    </div>
  );
};
