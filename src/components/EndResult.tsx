import { Button, Modal } from "flowbite-react";
import React from "react";
import { Round } from "../typings";
import { getEndResult } from "../utils/getEndResult";

interface Props {
  isOpen: boolean;
  close(): void;
  rounds: Round[];
}

export const EndResult: React.FC<Props> = ({ rounds, isOpen, close }) => {
  if (!rounds.length) {
    return null;
  }
  const endResult = getEndResult(rounds);
  return (
    <Modal show={isOpen} size="md" popup={true} onClose={close}>
      <Modal.Header>
        <div className="px-4">Eindstand:</div>
      </Modal.Header>
      <Modal.Body>
        {endResult.map((player, i) => {
          return (
            <div key={i} className={"flex gap-3 mb-4 items-center"}>
              <div
                className="py-4 w-14 text-center font-bold text-white rounded-full"
                style={{ backgroundColor: player.color }}
              >
                {player.endResult}
              </div>
              {player.name}
            </div>
          );
        })}
        <Button color={"info"} onClick={close}>
          Sluiten
        </Button>
      </Modal.Body>
    </Modal>
  );
};
