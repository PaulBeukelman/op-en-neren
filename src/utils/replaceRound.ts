import { Round } from "../typings";

export const replaceRound = (round: Round, rounds: Round[]): Round[] => {
  return [
    // Slice everything before round
    ...rounds.slice(0, round.index),
    // Add round that we update
    round,
    // Then everything after round
    ...rounds.slice(round.index + 1),
  ];
};
