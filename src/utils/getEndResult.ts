import { EndResultPlayer, Round } from "../typings";
import { getDataPointsForPlayer } from "./getDataPoints";

export const getEndResult = (rounds: Round[]): EndResultPlayer[] => {
  if (!rounds.length) {
    return [];
  }
  const playersWithScore: EndResultPlayer[] = rounds[0].playerOrder.map(
    (player) => {
      const datapoints = getDataPointsForPlayer(rounds, player.name);
      return {
        ...player,
        endResult: datapoints[datapoints.length - 1] || 0,
      };
    }
  );
  return playersWithScore.sort((a, b) => b.endResult - a.endResult);
};
