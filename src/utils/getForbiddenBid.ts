import { Round } from "../typings";

export const getForbiddenBid = (round: Round): number | null => {
  /**
   * Everything below 5 cards has no forbidden bid
   */
  if (round.cardCount < 5) {
    return null;
  }
  const lastPlayer = round.playerOrder[round.playerOrder.length - 1].name;
  const contracts = round.contracts;
  let totalBid = 0;
  (Object.keys(contracts || {}) as (number | string)[])
    // Filter out the last player
    .filter((player) => player !== lastPlayer)
    // Sum of all bids
    .forEach((player) => {
      totalBid = (contracts?.[player].bid || 0) + totalBid;
    });
  // If bigger then cardCount a player can say whatever he/she likes
  if (totalBid > round.cardCount) {
    return null;
  }
  return round.cardCount - totalBid;
};
