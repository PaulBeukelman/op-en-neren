import { Contract } from "../typings";

export const evaluateContract = (
  contract: Contract
): { points: number; isCorrect: boolean } => {
  const { bid, score } = contract;
  if (typeof bid === "undefined" || typeof score === "undefined") {
    return {
      points: 0,
      isCorrect: false,
    };
  }
  let points = 10 + bid * 3;
  if (bid > score) {
    points = (bid - score) * -3;
  }
  if (bid < score) {
    points = (score - bid) * -3;
  }
  return {
    points,
    isCorrect: score === bid,
  };
};
