import { Round } from "../typings";

export const getDataPointsForPlayer = (
  rounds: Round[],
  playerName: string
): number[] => {
  const dataPoints: number[] = [];
  rounds.forEach((round, i) => {
    const prevPoint = dataPoints[i - 1] || 0;
    const points = round.contracts?.[playerName]?.points;
    if (points) {
      dataPoints.push(prevPoint + points);
    }
  });
  return dataPoints;
};
