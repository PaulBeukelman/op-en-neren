import { Player } from "../typings";

export const getOrderedPlayers = (
  roundNumber: number,
  players: Player[]
): Player[] => {
  const startPlayerIndex = roundNumber % players.length;
  const startPlayer = players[startPlayerIndex];
  if (startPlayerIndex === 0) {
    return players;
  }
  return [
    // Start player first
    startPlayer,
    // Then slice every player after startplayer
    ...players.slice(startPlayerIndex + 1),
    // Then slice every player before startplayer
    ...players.slice(0, startPlayerIndex),
  ];
};
