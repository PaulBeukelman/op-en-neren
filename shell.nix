with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "op-en-neren";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    nodejs-16_x
  ];
}

